package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	banner("Go", 6)
	banner("G☺", 6)

	s := "G☺!"
	for i := range s {
		fmt.Print(i, " ")
	}
	fmt.Println()
	for i, c := range s {
		//fmt.Print(i, ":", c, ",")
		fmt.Printf("%d: %c, ", i, c)
	}
	fmt.Println()

	a, b := 1, "1"
	fmt.Printf("a=%v, b=%v\n", a, b)
	fmt.Printf("a=%#v, b=%#vi (%T)\n", a, b, b)

	//path := "C:\to\new\report.csv"
	path := `C:\to\new\report.csv` // raw string
	fmt.Println(path)

	// c := 'A' // c is a rune
}

/*
banner("Go", 6)

		Go
	  ------
*/
func banner(text string, width int) {
	// var padding = (len(text) - width) / 2
	// var padding
	// padding = (len(text) - width) / 2
	// padding := (width - len(text)) / 2
	padding := (width - utf8.RuneCountInString(text)) / 2
	for i := 0; i < padding; i++ {
		fmt.Print(" ")
	}
	fmt.Println(text)

	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
}
