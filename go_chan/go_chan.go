package main

import (
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 5; i++ {
		// FIX 2: Local loop variable
		i := i
		go func() {
			fmt.Println("gr:", i)
		}()
		/* FIX 1: Pass a parameter
		go func(n int) {
			fmt.Println("gr:", n)
		}(i)
		*/
		/* BUG: All goroutines use the same i from line 12
		go func() {
			fmt.Println("gr:", i)
		}()
		*/
	}

	time.Sleep(10 * time.Millisecond)

	ch := make(chan string)
	go func() {
		ch <- "hi" // send
	}()
	val := <-ch // receive
	fmt.Println(val)

	fmt.Println(sleepSort([]int{30, 10, 20})) // [10 20 30]

	go func() { // producer
		defer close(ch)
		for i := 0; i < 3; i++ {
			ch <- fmt.Sprintf("message #%d", i)
		}
	}()

	// consumer
	for msg := range ch {
		fmt.Println(msg)
	}
	/*
		The above for range is doing:
		for {
			msg, ok := <- ch
			if !ok {
				break
			}
			fmt.Println(msg)
		}
	*/

	msg := <-ch // ch is closed
	fmt.Printf("msg (closed): %q\n", msg)
	msg, ok := <-ch // ch is closed
	fmt.Printf("msg (closed): %q (ok=%v)\n", msg, ok)

	queue := make(chan int, 1) // buffered channel
	queue <- 7
	fmt.Println("sent to queue")

}

/* Channel semantics
- send/receive will block until opposite operation happen(*)
	- channel with buffer size N, has N non blocking sends
- receive from a closed channel will return zero value without blocking
	- use val, ok := <- ch
- sending to a closed channel will panic
- closing a closed channel will panic
- send/receive to a nil channel will block forever
*/

/*
for every value "n" in values, spin a goroutine that
  - sleep n milliseconds
  - send n over a channel

collect all values from the channel to a slice and return it
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	// fan out
	for _, n := range values {
		n := n
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}

	var sorted []int
	// for i := 0; i < len(values); i++ {
	for range values {
		val := <-ch
		sorted = append(sorted, val)
	}
	return sorted
}
