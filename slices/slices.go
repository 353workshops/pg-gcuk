package main

import "fmt"

func main() {
	// cart := []string{"lemon", "apple", "banana"}
	cart := []string{
		"lemon",
		"apple",
		"banana",
	}
	for i := range cart {
		fmt.Print(i, " ")
	}
	fmt.Println()
	for i, v := range cart {
		fmt.Print(i, " ", v, ", ")
	}
	fmt.Println()
	for _, v := range cart {
		fmt.Print(v, ", ")
	}
	fmt.Println()

	cart = append(cart, "bread")
	fmt.Println(cart)
	fruit := cart[:3] //slicing, half-open range
	fmt.Println("fruit:", fruit)
	cart[1] = "pear"
	fmt.Println("cart:", cart)
	fmt.Println("fruit:", fruit)

	var values []int
	for i := 0; i < 1_000; i++ {
		values = appendInt(values, i)
	}
	fmt.Println(values[:10])
}

func appendInt(values []int, val int) []int {
	i := len(values)
	if len(values) < cap(values) { // enough space in underlying array
		values = values[:len(values)+1]
	} else { // need to reallocate + copy
		size := 2 * (len(values) + 1)
		fmt.Println(cap(values), "->", size)
		s := make([]int, size)
		copy(s, values)
		values = s[:len(values)+1]
	}

	values[i] = val
	return values
}
