package main

import (
	"expvar"
	"fmt"
	"log"
	"net/http"
	"os"
)

var (
	// See /debug/vars
	healthCalls = expvar.NewInt("health.calls")
)

/*
	config

- command line: flag, cobra
- enviornment: os.Getenv, ardanlabs/conf ...
- config file: YAML, TOML, viper, ardanslabs/conf
*/
func main() {
	var api API
	http.HandleFunc("/health", api.healthHandler)

	addr := os.Getenv("HTTPD_ADDR")
	if addr == "" {
		addr = ":8080"
	}
	log.Printf("INFO: server starting on %s", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatalf("can't run on %s - %s", addr, err)
	}
}

func (s *API) healthHandler(w http.ResponseWriter, r *http.Request) {
	healthCalls.Add(1)
	if r.Method != http.MethodGet {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	// FIXME: Health checks
	fmt.Fprintln(w, "OK")
}

type API struct {
	// TODO: logger, db connection ...

}
