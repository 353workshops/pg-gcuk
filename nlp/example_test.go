package nlp_test

import (
	"fmt"

	"github.com/ardanlabs/nlp"
)

func ExampleTokenize() {
	text := "Who's on first?"
	fmt.Println(nlp.Tokenize(text))

	// Output:
	// [who on first]
}
