package nlp

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestTokenize(t *testing.T) {
	// setup()
	// defer teardown() // or use t.Cleanup
	text := "What's on second?"
	tokens := Tokenize(text)
	expected := []string{"what", "on", "second"}
	// if tokens != expected { // won't compile, can compare only to nil
	/*
		if !reflect.DeepEqual(expected, tokens) {
			t.Fatalf("expected: %#v, got %#v", expected, tokens)
		}
	*/
	require.Equal(t, expected, tokens)
}
